﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finals
{
    class PlayerList
    {
        public static List<Player> tplayer (int totalplayer)
        {
            List<Player> villager = new List<Player>();

            Villager Villager1 = new Villager();
            Villager1.Info("Khalel");
            villager.Add(Villager1);

            Villager Villager2 = new Villager();
            Villager2.Info("Michele");
            villager.Add(Villager2);

            Villager Villager3 = new Villager();
            Villager3.Info("Kurk");
            villager.Add(Villager3);

            Villager Villager4 = new Villager();
            Villager4.Info("Mike");
            villager.Add(Villager4);

            Villager Villager5 = new Villager();
            Villager5.Info("Brod");
            villager.Add(Villager5);

            Villager Villager6 = new Villager();
            Villager6.Info("John");
            villager.Add(Villager6);

            Villager Villager7 = new Villager();
            Villager7.Info("Bitancor");
            villager.Add(Villager7);

            Villager Villager8 = new Villager();
            Villager8.Info("Miley");
            villager.Add(Villager8);

            Villager Villager9 = new Villager();
            Villager9.Info("Wyth");
            villager.Add(Villager9);

            Villager Villager10 = new Villager();
            Villager10.Info("Proba");
            villager.Add(Villager10);

            Villager Villager11 = new Villager();
            Villager11.Info("Zelda");
            villager.Add(Villager11);

            Villager Villager12 = new Villager();
            Villager12.Info("Torch");
            villager.Add(Villager12);

            Villager Villager13 = new Villager();
            Villager13.Info("Pluto");
            villager.Add(Villager13);

            Villager Villager14 = new Villager();
            Villager14.Info("Denmy");
            villager.Add(Villager14);

            Villager Villager15 = new Villager();
            Villager15.Info("Staple");
            villager.Add(Villager15);

            switch (totalplayer)
            {
                case 5:
                    villager.Remove(Villager15);
                    villager.Remove(Villager14);
                    villager.Remove(Villager13);
                    villager.Remove(Villager12);
                    villager.Remove(Villager11);
                    villager.Remove(Villager10);
                    villager.Remove(Villager9);
                    villager.Remove(Villager8);
                    villager.Remove(Villager7);
                    villager.Remove(Villager6);
                    break;

                case 6:
                    villager.Remove(Villager15);
                    villager.Remove(Villager14);
                    villager.Remove(Villager13);
                    villager.Remove(Villager12);
                    villager.Remove(Villager11);
                    villager.Remove(Villager10);
                    villager.Remove(Villager9);
                    villager.Remove(Villager8);
                    villager.Remove(Villager7);
                    break;
                case 7:
                    villager.Remove(Villager15);
                    villager.Remove(Villager14);
                    villager.Remove(Villager13);
                    villager.Remove(Villager12);
                    villager.Remove(Villager11);
                    villager.Remove(Villager10);
                    villager.Remove(Villager9);
                    villager.Remove(Villager8);
                    break;

                case 8:
                    villager.Remove(Villager15);
                    villager.Remove(Villager14);
                    villager.Remove(Villager13);
                    villager.Remove(Villager12);
                    villager.Remove(Villager11);
                    villager.Remove(Villager10);
                    villager.Remove(Villager9);
                    break;

                case 9:
                    villager.Remove(Villager15);
                    villager.Remove(Villager14);
                    villager.Remove(Villager13);
                    villager.Remove(Villager12);
                    villager.Remove(Villager11);
                    villager.Remove(Villager10);
                    break;

                case 10:
                    villager.Remove(Villager15);
                    villager.Remove(Villager14);
                    villager.Remove(Villager13);
                    villager.Remove(Villager12);
                    villager.Remove(Villager11);
                    break;

                case 11:
                    villager.Remove(Villager15);
                    villager.Remove(Villager14);
                    villager.Remove(Villager13);
                    villager.Remove(Villager12);
                    break;

                case 12:
                    villager.Remove(Villager15);
                    villager.Remove(Villager14);
                    villager.Remove(Villager13);
                    break;

                case 13:
                    villager.Remove(Villager15);
                    villager.Remove(Villager14);
                    break;

                case 14:
                    villager.Remove(Villager15);
                    break;

                case 15:
                    break;
            }
            return villager;
        }
    }
}

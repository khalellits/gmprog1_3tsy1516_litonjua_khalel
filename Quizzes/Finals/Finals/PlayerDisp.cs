﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finals
{
    class PlayerDisp
    {
        public static void Display(List<Player> Village)
        {
            for (int i = 0; i < Village.Count(); i++)
            {
                Console.Write(i);
                Console.Write(". ");
                Village[i].GetInfo();
            }
        }
    }
}

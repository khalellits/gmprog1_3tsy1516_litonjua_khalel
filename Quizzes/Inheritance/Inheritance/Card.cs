﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inheritance
{
    class Card
    {
        public string name;
        public string race;

        public float Atk;
        public float Def;

        public int StarLevel;

        public SPELL_TYPE type;
        public CARD_TYPE ctype;

        public enum CARD_TYPE
        {
            Magic_card,
            Monster_card,
            Trap_card
        }
        public void PrintCardInfo()
        {
            Console.WriteLine(name, race, Atk, Def, StarLevel, type, ctype);
        }
    }
}

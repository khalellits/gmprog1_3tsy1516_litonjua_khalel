﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inheritance
{
    class MonsterCard : Card
    {
        CARD_TYPE type;
        public MonsterCard(string cardName, string cardRace, float cardAtk, float cardDef, int cardStarLvl, CARD_TYPE cardType)
        {
            name = cardName;
            race = cardRace;
            Atk = cardAtk;
            Def = cardDef;
            StarLevel = cardStarLvl;
            type = cardType;
        }
        public void printinfo()
        {
            Console.WriteLine(name);
        }
    }
}

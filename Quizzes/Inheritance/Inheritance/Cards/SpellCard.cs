﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inheritance
{
    
    public enum SPELL_TYPE
    { 
        NormalSpell,
        QuickPlayerSpell,
        CountinuousField,
        EquipSpell,
        FieldSpell,
        RitualSpell,
    }
    class SpellCard : Card
    {
        SPELL_TYPE type;
    }
}

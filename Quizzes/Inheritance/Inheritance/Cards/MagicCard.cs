﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inheritance
{
    class MagicCard : SpellCard
    {

        public MagicCard(string cardName, SPELL_TYPE spellType, CARD_TYPE cardType)
        {
            name = cardName;
            type = spellType;
            ctype = cardType;
        }
    }
}

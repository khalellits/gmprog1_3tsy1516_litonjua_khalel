﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Card> cardlist = new List<Card>();
            {
                Console.WriteLine("Your Deck");
                //Monster
                cardlist.Add(new MonsterCard("The Winged Dragon of Ra", "Immortal Pheonix", 300, 200, 5, Card.CARD_TYPE.Monster_card));
                cardlist.Add(new MonsterCard("Dark Magician", "Elf", 100, 100, 2, Card.CARD_TYPE.Monster_card));
                cardlist.Add(new MonsterCard("Gilford", "Human", 300, 500, 3, Card.CARD_TYPE.Monster_card));
                cardlist.Add(new MonsterCard("Exodia", "Human", 600, 200, 3, Card.CARD_TYPE.Monster_card));
                cardlist.Add(new MonsterCard("Blue Eyes White Dragon", "Dragon", 800, 500, 3, Card.CARD_TYPE.Monster_card));
                cardlist.Add(new MonsterCard("Skull Dragon", "Dragon", 1000, 200, 3, Card.CARD_TYPE.Monster_card));
                cardlist.Add(new MonsterCard("Khalel", "Human", 1000, 500, 10, Card.CARD_TYPE.Monster_card));
                cardlist.Add(new MonsterCard("John", "Elf", 200, 400, 3, Card.CARD_TYPE.Monster_card));
                cardlist.Add(new MonsterCard("Michale", "Human", 300, 500, 2, Card.CARD_TYPE.Monster_card));
                cardlist.Add(new MonsterCard("Skwalops", "Food", 100, 500, 5, Card.CARD_TYPE.Monster_card));

                //Magic
                cardlist.Add(new MagicCard("Change Of Heart", SPELL_TYPE.NormalSpell, Card.CARD_TYPE.Magic_card));
                cardlist.Add(new MagicCard("Rebellion", SPELL_TYPE.NormalSpell, Card.CARD_TYPE.Magic_card));
                cardlist.Add(new MagicCard("Card of Demise", SPELL_TYPE.NormalSpell, Card.CARD_TYPE.Magic_card));
                cardlist.Add(new MagicCard("Left Arm Offering", SPELL_TYPE.NormalSpell, Card.CARD_TYPE.Magic_card));
                cardlist.Add(new MagicCard("The True Name", SPELL_TYPE.NormalSpell, Card.CARD_TYPE.Magic_card));
                cardlist.Add(new MagicCard("Shrink", SPELL_TYPE.NormalSpell, Card.CARD_TYPE.Magic_card));
                cardlist.Add(new MagicCard("Symbol of Friendship", SPELL_TYPE.NormalSpell, Card.CARD_TYPE.Magic_card));
                cardlist.Add(new MagicCard("Scape Goat", SPELL_TYPE.NormalSpell, Card.CARD_TYPE.Magic_card));
                cardlist.Add(new MagicCard("Magic God", SPELL_TYPE.NormalSpell, Card.CARD_TYPE.Magic_card));
                cardlist.Add(new MagicCard("Flippit", SPELL_TYPE.NormalSpell, Card.CARD_TYPE.Magic_card));

                //Trap
                cardlist.Add(new TrapCard("Trap Hole Of Spikes", SPELL_TYPE.NormalSpell, Card.CARD_TYPE.Trap_card));
                cardlist.Add(new TrapCard("Nightmare Wheel", SPELL_TYPE.NormalSpell, Card.CARD_TYPE.Trap_card));
                cardlist.Add(new TrapCard("Ring of Destruction", SPELL_TYPE.NormalSpell, Card.CARD_TYPE.Trap_card));
                cardlist.Add(new TrapCard("Re Dyc Cle", SPELL_TYPE.NormalSpell, Card.CARD_TYPE.Trap_card));
                cardlist.Add(new TrapCard("Lunalight", SPELL_TYPE.NormalSpell, Card.CARD_TYPE.Trap_card));
                cardlist.Add(new TrapCard("Amorphage", SPELL_TYPE.NormalSpell, Card.CARD_TYPE.Trap_card));
                cardlist.Add(new TrapCard("Dinomist Eruption", SPELL_TYPE.NormalSpell, Card.CARD_TYPE.Trap_card));
                cardlist.Add(new TrapCard("Bug Emergency", SPELL_TYPE.NormalSpell, Card.CARD_TYPE.Trap_card));
                cardlist.Add(new TrapCard("Wonder XYZ", SPELL_TYPE.NormalSpell, Card.CARD_TYPE.Trap_card));
                cardlist.Add(new TrapCard("Rise to Full Height", SPELL_TYPE.NormalSpell, Card.CARD_TYPE.Trap_card));


                for (int i = 0; i < cardlist.Count; i++)
                {
                    Console.Write(i);
                    Console.Write(" ");
                    cardlist[i].PrintCardInfo();
                }
                Console.ReadKey();
            }
        }
    }
}
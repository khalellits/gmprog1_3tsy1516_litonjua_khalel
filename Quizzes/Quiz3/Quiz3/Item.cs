﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3
{
    class Item
    {
        string name;
        int price;

        public Item(string _name, int _price)
        {
            name = _name;
            price = _price;
        }

        public void PrintItemInfo()
        {
            Console.WriteLine(name + " = " + price);

        }

    }
}

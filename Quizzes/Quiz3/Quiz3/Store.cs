﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3
{
    class Store
    {
        List<Item> ItemList = new List<Item>();
        Player player = null;
        public Store(Player p)
        {
            player = p;
            Console.WriteLine("Welcome to Mall of Asia");
            ShowItems();
            GoldEx();
        }

        public void ShowItems()
        {
            ItemList.Add(new Item("Dagger", 25));
            ItemList.Add(new Item("Sword", 75));
            ItemList.Add(new Item("Axe", 100));
            ItemList.Add(new Item("Staff", 125));
            ItemList.Add(new Item("Hammer", 150));

            for (int i = 0; i < ItemList.Count; i++)
            {
                Console.Write(i);
                Console.Write(" ");
                ItemList[i].PrintItemInfo();
            }
        }
        public void GoldEx()
        {
            int choice = Convert.ToInt32(Console.ReadLine());
            switch (choice)
            {
                case 0:
                    if (player.ReturnGold() >= 25)
                    {
                        player.SubtractGold(25);
                        player.AddItemstoInv(0);
                    }
                    else
                    {
                        Console.WriteLine("You don't have enough gold!");
                    }
                    break;
                case 1:
                    if (player.ReturnGold() >= 75)
                    {
                        player.SubtractGold(75);
                        player.AddItemstoInv(1);
                    }
                    else
                    {
                        Console.WriteLine("You don't have enough gold!");
                    }
                    break;
                case 2:
                    if (player.ReturnGold() >= 100)
                    {
                        player.SubtractGold(100);
                        player.AddItemstoInv(2);
                    }
                    else
                    {
                        Console.WriteLine("You don't have enough gold!");
                    }
                    break;
                case 3:
                    if (player.ReturnGold() >= 125)
                    {
                        player.SubtractGold(125);
                        player.AddItemstoInv(3);
                    }
                    else
                    {
                        Console.WriteLine("You don't have enough gold!");
                    }
                    break;
                case 4:
                    if (player.ReturnGold() >= 150)
                    {
                        player.SubtractGold(150);
                        player.AddItemstoInv(4);
                    }
                    else
                    {
                        Console.WriteLine("You don't have enough gold!");
                    }
                    break;
            }
        }
    }
}
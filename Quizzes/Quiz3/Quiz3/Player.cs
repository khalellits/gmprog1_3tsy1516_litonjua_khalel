﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3
{
    class Player
    {
        string name;
        int gold = 500;

        List<Item> Inv = new List<Item>();
        int itemcount;

        public Player()
        {
            Console.WriteLine("Input Name: ");
            name = Console.ReadLine();

            Console.WriteLine("Gold: " + gold);
            
            Console.ReadKey();
            Console.Clear();
        }

        public void PrintStatInfo()
        {
            Console.WriteLine(name);
            Console.WriteLine("Gold: " + gold);
            itemcount = Inv.Count;
            Console.Write("Number of Items: " + itemcount);
        }

        public void AddGold(int gold1)
        {
            gold += gold1;
            Console.WriteLine("You earned: " + gold1);
            Console.WriteLine("Total: Gold: " + gold);
        }

        public void SubtractGold(int gold1)
        {
            gold -= gold1;
            Console.WriteLine("You spent: " + gold1);
            Console.WriteLine("Gold left: " + gold);
        }

        public int ReturnGold()
        {
            return gold;
        }

        public void ViewInventory()
        {
            if (Inv.Count != 0)
            {
                for (int i = 0; i < Inv.Count; i++)
                {
                    Console.Write(i);
                    Console.Write(". ");
                    Inv[i].PrintItemInfo();
                }
            }
            else
            {
                Console.WriteLine("You didn't buy any items");
            }
            Console.ReadKey();
        }

        public void AddItemstoInv(int choice)
        {
            switch (choice)
            {
                case 0:
                    Inv.Add(new Item("Dagger", 25));
                    break;
                case 1:
                    Inv.Add(new Item("Sword", 75));
                    break;
                case 2:
                    Inv.Add(new Item("Axe", 100));
                    break;
                case 3:
                    Inv.Add(new Item("Staff", 125));
                    break;
                case 4:
                    Inv.Add(new Item("Hammer", 150));
                    break;
            }
        }
    }
}
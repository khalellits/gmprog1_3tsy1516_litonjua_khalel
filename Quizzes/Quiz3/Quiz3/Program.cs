﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3
{
    class Program
    {
        static void Main(string[] args)
        {
            Player player = new Player();
            int choice = 0;

            for (;;)
            {
                Console.Clear();
                player.PrintStatInfo();
                Console.WriteLine(" ");
                Console.WriteLine("1. Visit the store");
                Console.WriteLine("2. Travel");
                Console.WriteLine("3. View Inventory");
                Console.WriteLine("4. Exit");

                choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Store shop = new Store(player);
                        Console.ReadKey();
                        break;
                    case 2:
                        Random rnd = new Random();
                        player.AddGold(rnd.Next(25, 100));
                        Console.ReadKey();
                        break;
                    case 3:
                        player.ViewInventory();
                        Console.ReadKey();
                        break;
                    case 4:
                        Environment.Exit(0);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
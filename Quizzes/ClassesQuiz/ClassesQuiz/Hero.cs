﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesQuiz
{
    class Hero
    {
        string name;
        int stre;
        int agil;
        int inte;
        public Hero()
        {
            Console.Write("Input your name: ");
            name = Console.ReadLine();
            Console.Write("Input your strength: ");
            stre = Convert.ToInt32(Console.ReadLine());
            Console.Write("Input your agility: ");
            agil = Convert.ToInt32(Console.ReadLine());
            Console.Write("Input your intelligence: ");
            inte = Convert.ToInt32(Console.ReadLine());

            Console.ReadKey();
        }
        public void PrintUserInfo()
        {
            Console.WriteLine("");
            Console.WriteLine("Hero Name: " + name);
            Console.WriteLine("Strength: " + stre);
            Console.WriteLine("Agility: " + agil);
            Console.WriteLine("Intelligence: " + inte);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesQuiz
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Hero> herolist = new List<Hero>();
            List<Villain> villainlist = new List<Villain>();

            int choice = 0;
            for (;;)
            {
                Console.Clear();
                Console.WriteLine("1. Add Hero 2. Add Villain 3. Print All Hero And Villain 3. Exit");
                choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("Add hero");
                        Hero hero = new Hero();
                        herolist.Add(hero);
                        break;
                    case 2:
                        Console.Clear();
                        Console.WriteLine("Add villain");
                        Villain villain = new Villain();
                        villainlist.Add(villain);
                        break;
                    case 3:
                        Console.Clear();
                        Console.WriteLine("Print All Hero And Villain");

                        for (int i = 0; i < herolist.Count; i++)
                        {
                            herolist[i].PrintUserInfo();
                        }

                        for (int i = 0; i < villainlist.Count; i++)
                        {
                            villainlist[i].PrintUserInfo();
                        }
                        Console.ReadKey();
                        break;
                    case 4:
                        Environment.Exit(0);
                        break;
                    default:
                        break;
                }
            }
            Console.ReadKey();
        }
    }
}
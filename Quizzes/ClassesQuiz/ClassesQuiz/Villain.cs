﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesQuiz
{
    class Villain
    {
        string name;
        int defe;
        int wisd;
        int dext;
        public Villain()
        {
            Console.Write("Input your name: ");
            name = Console.ReadLine();
            Console.Write("Input your defence: ");
            defe = Convert.ToInt32(Console.ReadLine());
            Console.Write("Input your wisdom: ");
            wisd = Convert.ToInt32(Console.ReadLine());
            Console.Write("Input your dexterity: ");
            dext = Convert.ToInt32(Console.ReadLine());

            Console.ReadKey();
        }
        public void PrintUserInfo()
        {
            Console.WriteLine("");
            Console.WriteLine("Villain Name: " + name);
            Console.WriteLine("Strength: " + defe);
            Console.WriteLine("Agility: " + wisd);
            Console.WriteLine("Intelligence: " + dext);
        }
    }
}
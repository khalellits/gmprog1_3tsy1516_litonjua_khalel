﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesQuiz
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Hero> herolist = new List<Hero>();
            int choice = 0;

            for (;;)
            {
                Console.Clear();
                Console.WriteLine("1. Add Hero 2. Print All Hero 3. Exit");
                choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("Add hero");
                        Hero hero = new Hero();
                        herolist.Add(hero);
                        break;
                    case 2:
                        Console.Clear();
                        Console.WriteLine("Print all heroes");

                        for (int i = 0; i < herolist.Count; i++)
                        {
                            herolist[i].PrintUserInfo();
                        }
                        Console.ReadKey();
                        break;
                    case 3:
                        Environment.Exit(0);
                        break;
                    default:
                        break;
                }
            }
            Console.ReadKey();
        }

        class Hero
        {
            string name;
            int stre;
            int agil;
            int inte;
            public Hero()
            {
                Console.Write("Input your name: ");
                name = Console.ReadLine();
                Console.Write("Input your strength: ");
                stre = Convert.ToInt32(Console.ReadLine());
                Console.Write("Input your agility: ");
                agil = Convert.ToInt32(Console.ReadLine());
                Console.Write("Input your intelligence: ");
                inte = Convert.ToInt32(Console.ReadLine());

                Console.ReadKey();
            }
            public void PrintUserInfo()
            {
                Console.WriteLine("Name: " + name);
                Console.WriteLine("Strength: " + stre);
                Console.WriteLine("Agility: " + agil);
                Console.WriteLine("Intelligence: " + inte);
            }
        }
    }
}
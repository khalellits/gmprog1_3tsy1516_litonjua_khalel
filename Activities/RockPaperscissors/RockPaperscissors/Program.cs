﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JacknPoy
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Let's play Rock, Paper or Scissors");
            Console.WriteLine("Please pick.");
            gameFunc();
        }

        public static void gameFunc()
        {
            Random numberGen = new Random();

            string rock = "ROCK";
            string paper = "PAPER";
            string scissors = "SCISSORS";
            string choice = Console.ReadLine();

            int random = numberGen.Next(1, 4);
            choice = choice.ToUpper();

            switch (random)
            {
                case 1:
                    if (choice == rock)
                    {
                        Console.WriteLine("Your choice is: " + choice);
                        Console.WriteLine("Opponent's choice is: " + rock);
                        Console.WriteLine("It's a tie try again!");
                        nextround();

                    }
                    else if (choice == paper)
                    {
                        Console.WriteLine("Your choice is: " + choice);
                        Console.WriteLine("Opponent's choice is: " + rock);
                        Console.WriteLine("You win!");
                    }
                    else if (choice == scissors)
                    {
                        Console.WriteLine("Your choice is: " + choice);
                        Console.WriteLine("Opponent's choice is: " + rock);
                        lose();
                    }
                    break;
                case 2:
                    if (choice == rock)
                    {
                        Console.WriteLine("Your choice is: " + choice);
                        Console.WriteLine("Opponent's choice is: " + paper);
                        lose();
                    }
                    else if (choice == paper)
                    {
                        Console.WriteLine("Your choice is: " + choice);
                        Console.WriteLine("Opponent's choice is: " + paper);
                        Console.WriteLine("It's a tie try again!");
                        nextround();
                    }
                    else if (choice == scissors)
                    {
                        Console.WriteLine("Your choice is: " + choice);
                        Console.WriteLine("Opponent's choice is: " + paper);
                        Console.WriteLine("You win!");
                    }
                    break;
                default:
                    if (choice == rock)
                    {
                        Console.WriteLine("Your choice is: " + choice);
                        Console.WriteLine("Opponent's choice is: " + scissors);
                        Console.WriteLine("You win!");
                    }
                    else if (choice == paper)
                    {
                        Console.WriteLine("Your choice is: " + choice);
                        Console.WriteLine("Opponent's choice is: " + scissors);
                        lose();
                    }
                    else if (choice == scissors)
                    {
                        Console.WriteLine("Your choice is: " + choice);
                        Console.WriteLine("Opponent's choice is: " + scissors);
                        Console.WriteLine("It's a tie try again!");
                        nextround();
                    }
                    break;
            }
            Console.ReadLine();
            
        }
        public static void lose()
        {
            Console.WriteLine("You lose! Thanks for playing try again next time.");
        }
        public static void nextround()
        {
            bool keepPlaying = true;
            while (keepPlaying);
        }
    }
}


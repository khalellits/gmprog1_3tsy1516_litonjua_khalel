﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Circumference_Activity
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Circumference Calculator");
            input();
            Console.ReadKey();
        }

        static void input()
        {
            Console.WriteLine("Input the name of your circle:");
            string name = Console.ReadLine();

            Console.WriteLine("Input the Radius of the Circle: ");
            int r = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("The name of your circle is: " + name);
            Console.WriteLine("The circumference of the circle is: " + circum(r));
        }
        static double circum(double r)
        {
            double pi = Math.PI;
            double area = 2 * pi * r;
            return area;
        }
    }
}
